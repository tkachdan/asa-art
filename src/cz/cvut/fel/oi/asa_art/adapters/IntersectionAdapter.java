package cz.cvut.fel.oi.asa_art.adapters;

import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import cz.cvut.fel.oi.asa_art.R;
import cz.cvut.fel.oi.asa_art.model.Intersection;

public class IntersectionAdapter extends ArrayAdapter<Intersection> {

	public IntersectionAdapter(Context context, List<Intersection> objects) {
		super(context, R.layout.intersection_layout, objects);
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) this.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.intersection_layout, parent,
				false);
		TextView latitude = (TextView) rowView.findViewById(R.id.latitude_val);
		TextView longitude = (TextView) rowView
				.findViewById(R.id.longitude_val);
		TextView date = (TextView) rowView
				.findViewById(R.id.date_of_intersection);

		latitude.setText(Double.toString(this.getItem(position).getLat()));
		longitude.setText(Double.toString(this.getItem(position).getLon()));

		date.setText(new SimpleDateFormat().format(this.getItem(position)
				.getTimestamp()));

		return rowView;
	}

}
