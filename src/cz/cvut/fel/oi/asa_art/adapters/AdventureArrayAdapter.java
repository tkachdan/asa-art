package cz.cvut.fel.oi.asa_art.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import cz.cvut.fel.oi.asa_art.R;
import cz.cvut.fel.oi.asa_art.model.Adventure;

public class AdventureArrayAdapter extends ArrayAdapter<Adventure> {

	public AdventureArrayAdapter(Context context, Adventure[] objects) {
		super(context, R.layout.adventure_layout, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) this.getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.adventure_layout, parent,
				false);
		TextView textTitle = (TextView) rowView
				.findViewById(R.id.adventureViewTitle);
		TextView textStarted = (TextView) rowView
				.findViewById(R.id.adventureViewStarted);
		TextView textFrequency = (TextView) rowView
				.findViewById(R.id.adventureViewFrequency);

		textTitle.setText(this.getItem(position).getBat_species());
		textStarted.setText("Started on "
				+ this.getItem(position).getCreated_on().toString());
		textFrequency.setText(Double.toString(this.getItem(position)
				.getFrequency()) + " MHz");

		return rowView;
	}

}
