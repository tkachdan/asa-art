package cz.cvut.fel.oi.asa_art.fragment;

import cz.cvut.fel.oi.asa_art.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;

public class CreateAdventureDialogFragment
	extends DialogFragment {
	
	public interface CreateAdventureDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
	}
	
	// Use this instance of the interface to deliver action events
	CreateAdventureDialogListener mListener;
    
    // Override the Fragment.onAttach() method to instantiate the CreateAdventureDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (CreateAdventureDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement CreateAdventureDialogListener");
        }
    }
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    // Get the layout inflater
	    LayoutInflater inflater = getActivity().getLayoutInflater();

	    // set view from inflated layout
	    builder.setView(inflater.inflate(R.layout.fragment_start_new_adventure, null))
	    // Add action buttons
	    	.setPositiveButton(R.string.start_adventure_create, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// do a sign in
					
					// Send the positive button event back to the host activity
                    mListener.onDialogPositiveClick(CreateAdventureDialogFragment.this);
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// Send the positive button event back to the host activity
                    mListener.onDialogNegativeClick(CreateAdventureDialogFragment.this);
					CreateAdventureDialogFragment.this.getDialog().cancel();
				}
			});
	    
	    return builder.create();
	}
}
