package cz.cvut.fel.oi.asa_art.model;

import java.io.Serializable;
import java.util.Date;

import com.google.android.gms.maps.model.LatLng;

import android.location.Location;

public class Intersection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int adventure_id;
	private Date timestamp;
	private int seq_num;
	private double lat;
	private double lon;
	private double accuracy_hor;
	
	Intersection() {}
	public Intersection(int adventure_id, Date timestamp, int seq_num,
			Location location) {
		super();
		this.adventure_id = adventure_id;
		this.timestamp = timestamp;
		this.seq_num = seq_num;
		this.lat = location.getLatitude();
		this.lon = location.getLongitude();
		this.accuracy_hor = location.getAccuracy();
	}
	public Intersection(int adventure_id, Date timestamp, int seq_num,
			LatLng location) {
		super();
		this.adventure_id = adventure_id;
		this.timestamp = timestamp;
		this.seq_num = seq_num;
		this.lat = location.latitude;
		this.lon = location.longitude;
		this.accuracy_hor = 0;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAdventure_id() {
		return adventure_id;
	}
	public void setAdventure_id(int adventure_id) {
		this.adventure_id = adventure_id;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public int getSeq_num() {
		return seq_num;
	}
	public void setSeq_num(int seq_num) {
		this.seq_num = seq_num;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public double getAccuracy_hor() {
		return accuracy_hor;
	}
	public void setAccuracy_hor(double accuracy_hor) {
		this.accuracy_hor = accuracy_hor;
	}
}
