package cz.cvut.fel.oi.asa_art.model;

import java.io.Serializable;
import java.util.Date;

public class Measurement implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int adventure_id;
	private int initiating_team_id;
	private Date start_time;
	private int duration; // in minutes
	private boolean ended;
	
	Measurement() {}
	public Measurement(int adventure_id, int initiating_team_id,
			Date start_time, int duration, boolean ended) {
		super();
		this.adventure_id = adventure_id;
		this.initiating_team_id = initiating_team_id;
		this.start_time = start_time;
		this.duration = duration;
		this.ended = ended;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdventure_id() {
		return adventure_id;
	}

	public void setAdventure_id(int adventure_id) {
		this.adventure_id = adventure_id;
	}

	public int getInitiating_team_id() {
		return initiating_team_id;
	}

	public void setInitiating_team_id(int initiating_team_id) {
		this.initiating_team_id = initiating_team_id;
	}

	public Date getStart_time() {
		return start_time;
	}

	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

}
