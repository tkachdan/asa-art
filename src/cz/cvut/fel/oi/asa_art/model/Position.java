package cz.cvut.fel.oi.asa_art.model;

import java.io.Serializable;
import java.util.Date;

import android.location.Location;

public class Position implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int adventure_id;
	private int team_id;
	private Date timestamp;
	private int seq_num;
	
	private double lat;
	private double lon;
	private double alt;
	private float accuracy_hor;

	private double direction;
	private double accuracy_dir;
	
	private boolean was_measured;	
	
	public Position(int adventure_id, int team_id, int seq_num,
			Date timestamp,
			Location location, float direction, float accuracy_dir,
			boolean was_measured) {
		super();
		this.adventure_id = adventure_id;
		this.team_id = team_id;
		this.timestamp = timestamp;
		this.seq_num = seq_num;
		this.lat = location.getLatitude();
		this.lon = location.getLongitude();
		this.alt = location.getAltitude();
		this.accuracy_hor = location.getAccuracy();
		this.direction = direction;
		this.accuracy_dir = accuracy_dir;
		this.was_measured = was_measured;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAdventure_id() {
		return adventure_id;
	}

	public void setAdventure_id(int adventure_id) {
		this.adventure_id = adventure_id;
	}

	public int getTeam_id() {
		return team_id;
	}

	public void setTeam_id(int team_id) {
		this.team_id = team_id;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public int getSeq_num() {
		return seq_num;
	}

	public void setSeq_num(int seq_num) {
		this.seq_num = seq_num;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getAlt() {
		return alt;
	}

	public void setAlt(double alt) {
		this.alt = alt;
	}

	public double getAccuracy_hor() {
		return accuracy_hor;
	}

	public void setAccuracy_hor(float accuracy_hor) {
		this.accuracy_hor = accuracy_hor;
	}

	public double getDirection() {
		return direction;
	}

	public void setDirection(double direction) {
		this.direction = direction;
	}

	public double getAccuracy_dir() {
		return accuracy_dir;
	}

	public void setAccuracy_dir(double accuracy_dir) {
		this.accuracy_dir = accuracy_dir;
	}

	public boolean isWas_measured() {
		return was_measured;
	}

	public void setWas_measured(boolean was_measured) {
		this.was_measured = was_measured;
	}
}
