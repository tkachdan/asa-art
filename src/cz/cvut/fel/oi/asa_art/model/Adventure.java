package cz.cvut.fel.oi.asa_art.model;

import java.io.Serializable;
import java.util.Date;

public class Adventure implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String title;
	private String bat_species;
	private double frequency;
	private Date created_on;
	private Date ended_on;
	
	private int admin_team_id;
	
	@SuppressWarnings("unused")
	private Adventure() {}
	
	public Adventure(String title, String bat_species, double frequency,
			int admin_team_id) {
		super();
		this.title = title;
		this.bat_species = bat_species;
		this.frequency = frequency;
		this.admin_team_id = admin_team_id;
		
		this.created_on = new Date();
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBat_species() {
		return bat_species;
	}

	public void setBat_species(String bat_species) {
		this.bat_species = bat_species;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	
	public Date getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public Date getEnded_on() {
		return ended_on;
	}

	public void setEnded_on(Date ended_on) {
		this.ended_on = ended_on;
	}

	public int getAdmin_team_id() {
		return admin_team_id;
	}

	public void setAdmin_team_id(int admin_team_id) {
		this.admin_team_id = admin_team_id;
	}
	
	
}
