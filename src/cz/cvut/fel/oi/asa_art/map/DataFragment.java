package cz.cvut.fel.oi.asa_art.map;

import java.util.List;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cz.cvut.fel.oi.asa_art.R;
import cz.cvut.fel.oi.asa_art.adapters.IntersectionAdapter;
import cz.cvut.fel.oi.asa_art.model.Adventure;
import cz.cvut.fel.oi.asa_art.model.Intersection;

public class DataFragment extends Fragment {

	TextView adventureName;
	TextView adventureID;
	TextView batSpecies;
	TextView frequency;
	ListView intersectionsList;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.data_fragment, container, false);

		//Adventure adv = (Adventure) getArguments().getSerializable(
		//		"adventureData");
		Adventure adv = MapScreenActivity.adv;

		adventureName = (TextView) view.findViewById(R.id.adventure_name);
		adventureName.setText(adv.getTitle());
		adventureID = (TextView) view.findViewById(R.id.adventure_id);
		adventureID.setText(Integer.toString(adv.getId()));
		batSpecies = (TextView) view.findViewById(R.id.bat_species);
		batSpecies.setText(adv.getBat_species());
		frequency = (TextView) view.findViewById(R.id.frequency_value);
		frequency.setText(Double.toString(adv.getFrequency()));
		intersectionsList = (ListView) view.findViewById(R.id.intersections);

		//List<Intersection> intersections = (List<Intersection>) getArguments()
		//		.getSerializable("intersections");
		List<Intersection> intersections = MapScreenActivity.intersections;
		
		if (intersections != null) {

			ArrayAdapter<Intersection> adapter = new IntersectionAdapter(
					this.getActivity(), intersections);
			intersectionsList = (ListView) view
					.findViewById(R.id.intersections);
			intersectionsList.setAdapter(adapter);
		}

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of
		// actions in the action bar.
		setHasOptionsMenu(false);
	}
}