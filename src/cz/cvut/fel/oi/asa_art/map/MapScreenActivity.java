package cz.cvut.fel.oi.asa_art.map;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import cz.cvut.fel.oi.asa_art.LoginActivity;
import cz.cvut.fel.oi.asa_art.R;
import cz.cvut.fel.oi.asa_art.SettingsActivity;
import cz.cvut.fel.oi.asa_art.StartScreenActivity;
import cz.cvut.fel.oi.asa_art.model.Adventure;
import cz.cvut.fel.oi.asa_art.model.Intersection;
import cz.cvut.fel.oi.asa_art.model.Measurement;
import cz.cvut.fel.oi.asa_art.model.Position;
import cz.cvut.fel.oi.asa_art.model.Team;
import cz.cvut.fel.oi.asa_art.repository.GetJson;
import cz.cvut.fel.oi.asa_art.repository.PostJson;

public class MapScreenActivity extends FragmentActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks,
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private GoogleMap mMap;

	boolean zoomToMyLocationAtStart = false;
	Marker markerPosition2Team;
	Handler handler = new Handler();
	Thread postThread;
	Runnable syncRunnable;
	private int adv_id;

	public static Adventure adv;
	private Team team;

	private MapFragment mapFragment;
	private FragmentManager fragmentManager;
	private DataFragment dataFragment;
	private Fragment fragment;
	private TextView displayTime;
	private TimePickerDialog timePickerDialog;
	private boolean isTimeSet = false;
	private int seq_num = 0;
	Location mCurrentLocation;
	LocationClient mLocationClient;
	private CountDownTimer countDownTimer = null;
	private final long interval = 1 * 1000;
	int counter = 0;
	private NavigationDrawerFragment mNavigationDrawerFragment;
	int drawer_selected_item;

	/**
	 * Collections for positions of teams.
	 */
	private Map<Integer, Position> teamPositions = new HashMap<Integer, Position>();
	/**
	 * Collection of markers for each team.
	 */
	private Map<Integer, Marker> teamMarkers = new HashMap<Integer, Marker>();

	/**
	 * Collection of polylines for each team.
	 */
	private Map<Integer, Polyline> teamPolylines = new HashMap<Integer, Polyline>();
	/**
	 * Collection for intersections. Static because we are in a time pressure to think of something better.
	 */
	public static List<Intersection> intersections = new ArrayList<Intersection>();

	public static float azimuth;
	public TextView onlineIndicator;

	private static SensorManager sensorService;
	private Sensor sensor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map_screen);

		displayTime = (TextView) findViewById(R.id.remainning_time);
		displayTime.setVisibility(View.INVISIBLE);

		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras == null) {
				adv = null;
				displayToast("No adventure was put");
			} else {
				adv = (Adventure) extras.getSerializable("adv");
				adv_id = adv.getId();
				Log.d("ID", String.valueOf(adv_id));
				team = (Team) extras.getSerializable(LoginActivity.EXTRA_TEAM);
			}
		} else {
			adv = (Adventure) savedInstanceState.getSerializable("adv");

			team = (Team) savedInstanceState
					.getSerializable(LoginActivity.EXTRA_TEAM);
			getActionBar().setTitle(adv.getTitle());
		}
		sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		sensor = sensorService.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		if (sensor != null) {
			sensorService.registerListener(mySensorEventListener, sensor,
					SensorManager.SENSOR_DELAY_NORMAL);
			Log.i("Compass MainActivity", "Registerered for ORIENTATION Sensor");
		} else {
			Log.e("Compass MainActivity", "Registerered for ORIENTATION Sensor");
			Toast.makeText(this, "ORIENTATION Sensor not found",
					Toast.LENGTH_LONG).show();
			finish();
		}

		mLocationClient = new LocationClient(this, this, this);

		dataFragment = new DataFragment();
		mapFragment = new MapFragment();
		displayTime = (TextView) findViewById(R.id.remainning_time);
		this.drawer_selected_item = -1;
		int new_id = 0;
		if (savedInstanceState != null)
			new_id = savedInstanceState.getInt("selected_item", 2);
		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		getTitle();
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		onNavigationDrawerItemSelected(new_id);

		onlineIndicator = (TextView) findViewById(R.id.online_indicator);
		
		// get intersections for the adventure from the web
		getIntersectionInfoFromNet();

	}

	private LatLng getCoordinatesFromDistanceAndAzimut(double latitude,
			double longitude, int distance, float azimuth) {

		double earthRadius = 6378.1;

		double constToRadian = Math.PI / 180;
		double constToDegrees = 180 / Math.PI;
		double directionRadian = constToRadian * azimuth;
		double distaneceToEarthRatio = distance / earthRadius;

		double latitudeRadians = latitude * constToRadian;
		double longitudeRadians = longitude * constToRadian;

		double dest_latitude = Math.asin(Math.sin(latitudeRadians)
				* Math.cos(distaneceToEarthRatio) + Math.cos(latitudeRadians)
				* Math.sin(distaneceToEarthRatio) * Math.cos(directionRadian));

		double dest_longtitude = longitudeRadians
				+ Math.atan2(
						Math.sin(directionRadian)
								* Math.sin(distaneceToEarthRatio)
								* Math.cos(latitudeRadians),
						Math.cos(distaneceToEarthRatio)
								- Math.sin(latitudeRadians)
								* Math.sin(dest_latitude));

		dest_latitude = dest_latitude * constToDegrees;
		dest_longtitude = dest_longtitude * constToDegrees;

		return new LatLng(dest_latitude, dest_longtitude);
	}

	private LatLng coordinateOfIntersectionOf2Lines(double pos1StartX,
			double pos1StartY, double pos1EndX, double pos1EndY,
			double pos2StartX, double pos2StartY, double pos2EndX,
			double pos2EndY) {

		if ((((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY)) - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX))) == 0) {
			return null;
		}
		double x = ((((pos1StartX * pos1EndY) - (pos1StartY * pos1EndX)) * (pos2StartX - pos2EndX)) - ((pos1StartX - pos1EndX) * ((pos2StartX * pos2EndY) - (pos2StartY * pos2EndX))))
				/ (((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY)) - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX)));

		double y = ((((pos1StartX * pos1EndY) - (pos1StartY * pos1EndX)) * (pos2StartY - pos2EndY)) - ((pos1StartY - pos1EndY) * ((pos2StartX * pos2EndY) - (pos2StartY * pos2EndX))))
				/ (((pos1StartX - pos1EndX) * (pos2StartY - pos2EndY)) - ((pos1StartY - pos1EndY) * (pos2StartX - pos2EndX)));

		return new LatLng(x, y);
	}

	private LatLng coordinateOfIntersectionOf2Lines(LatLng point1S,
			LatLng point1E, LatLng point2S, LatLng point2E) {
		return coordinateOfIntersectionOf2Lines(point1S.latitude,
				point1S.longitude, point1E.latitude, point1E.longitude,
				point2S.latitude, point2S.longitude, point2E.latitude,
				point2E.longitude);
	}

	@Override
	protected void onStart() {
		super.onStart();
		mapFragment.getMap();
		mapFragment.getMap().setMapType(GoogleMap.MAP_TYPE_HYBRID);
		mapFragment.getMap().setPadding(0, 0, 0, 100);
		mapFragment.getMap().setMyLocationEnabled(true);
		mapFragment.getMap().getUiSettings().setCompassEnabled(true);
		mapFragment.getMap().getUiSettings().setMyLocationButtonEnabled(true);
		mMap = mapFragment.getMap();
		mLocationClient.connect();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mLocationClient.disconnect();
		handler.removeCallbacks(syncRunnable);
	}

	@Override
	protected void onStop() {
		super.onStop();
		mLocationClient.disconnect();
		handler.removeCallbacks(syncRunnable);
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		// TODO Auto-generated method stub
		super.onAttachFragment(fragment);
		fragment.setRetainInstance(true);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		// outState.putAll(outState);
		outState.putSerializable("adv", adv);
		outState.putSerializable(LoginActivity.EXTRA_TEAM, team);
		outState.putInt("selected_item", drawer_selected_item);
		mLocationClient.disconnect();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mLocationClient.disconnect();
		handler.removeCallbacks(syncRunnable);
		mapFragment = null;

		mMap = null;
		if (sensor != null) {
			sensorService.unregisterListener(mySensorEventListener);
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {

		if (drawer_selected_item == position) {
			return;
		}

		fragmentManager = getFragmentManager();

		switch (position) {
		case 0:
			drawer_selected_item = 0;

			if (counter > 1) {
				mapFragment.getMap().setMapType(GoogleMap.MAP_TYPE_HYBRID);
				mapFragment.getMap().setPadding(0, 0, 0, 100);
				fragmentManager.popBackStack();
			}
			fragment = mapFragment;

			break;
		case 1:
			drawer_selected_item = 1;
			Bundle adventureBundle = new Bundle();
			adventureBundle.putSerializable("adventureData", adv);
			dataFragment.setArguments(adventureBundle);
			if (!intersections.isEmpty()) {
				Bundle intersectionsBundle = new Bundle();
				intersectionsBundle.putSerializable("intersections",
						(Serializable) intersections);
				dataFragment.setArguments(intersectionsBundle);
			}
			fragment = dataFragment;
			break;
		}
		counter++;
		fragment.setRetainInstance(true);
		drawer_selected_item = position;

		if (fragment != null) {
			fragmentManager.beginTransaction().addToBackStack("name")
					.replace(R.id.container, fragment).commit();
		}

	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(adv.getBat_species());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.

			if (drawer_selected_item == 0) {
				getMenuInflater().inflate(R.menu.map_screen, menu);
			} else {
				getMenuInflater().inflate(R.menu.global, menu);
			}

			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		mMap = mapFragment.getMap();
		int id = item.getItemId();

		if (id == R.id.start_meassurment) {
			if (isTimeSet) {

				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						this);
				// set title
				alertDialogBuilder.setTitle("Measurement");
				// set dialog message
				alertDialogBuilder
						.setMessage("You cannot start another measurement while one is in progress.")
						.setCancelable(true)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				// show it
				alertDialog.show();
			} else {
				openTimePickerDialog(true);
			}
		}

		if (id == R.id.hybrid) {
			mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
			return true;
		}
		if (id == R.id.satellite) {
			mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
			return true;
		}
		if (id == R.id.normal) {
			mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void drawTemporaryLine() {

	}

	/**
	 * Sets a new measurement date, time.
	 */
	private void setMeasurementTime(Date time) {
		Calendar calNow = Calendar.getInstance();
		calNow.setTime(time);

		setMeasurementTime(calNow.get(Calendar.HOUR_OF_DAY),
				calNow.get(Calendar.MINUTE));
	}

	/**
	 * Sets a new measurement time.
	 */
	private void setMeasurementTime(int hourOfDay, int minute) {
		Calendar calNow = Calendar.getInstance();
		Calendar calSet = Calendar.getInstance();

		calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
		calSet.set(Calendar.MINUTE, minute);
		calSet.set(Calendar.SECOND, 0);

		// Today Set time not yet passed
		long milliseconds = calSet.getTimeInMillis() - calNow.getTimeInMillis();
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

		if (milliseconds < 0) {
			displayToast("New measurement can`t start in the past ;)");
			isTimeSet = false;
			return;
		}

		displayTime.setVisibility(View.VISIBLE);
		displayTime.setText(String.valueOf(minutes));
		displayToast("Measurement will start in : "
				+ Integer.toString(minutes + (hours * 60)) + " minutes");
		startCountDownTimer(milliseconds);
		isTimeSet = true;
	}

	private void sendIntersectionInfoToNet(Intersection intersection) {
		AsyncTask<Intersection, Integer, Intersection> t = new AsyncTask<Intersection, Integer, Intersection>() {
			@Override
			protected Intersection doInBackground(Intersection... params) {

				Intersection inters = params[0]; 
				
				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
				Gson gson = gsonBuilder.create();
				String jsonToPost = gson.toJson(inters, Intersection.class);
				PostJson postJson = new PostJson(
						StartScreenActivity.ADVENTURE_URL + "/" + adv_id
								+ "/intersections", jsonToPost);
				postJson.run();
				// getting returned int
				JsonObject returnedJson = 
						new JsonParser().parse(postJson.getResponce())
						.getAsJsonObject();
				inters.setId(returnedJson.get("id").getAsInt());
				return inters;
			}

			@Override
			protected void onPostExecute(Intersection result) {
				super.onPostExecute(result);
			}
		};

		t.execute(intersection);
	}

	/**
	 * Gets the information about previous measurements from the Internet.
	 */
	private void getIntersectionInfoFromNet() {
		AsyncTask<Adventure, Integer, String> t = new AsyncTask<Adventure, Integer, String>() {
			@Override
			protected String doInBackground(Adventure... params) {
				Adventure adv_l = params[0];
				GetJson getJson = new GetJson();
				getJson.setUrl(
						StartScreenActivity.ADVENTURE_URL + "/" + adv_l.getId()
								+ "/intersections");
				getJson.run();
				return getJson.getJson();
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				/*
				 * if (!result.equals(null)) { continueRunning(null); return; }
				 */

				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
				Gson gson = gsonBuilder.create();
				try {
					JsonArray jsonResponse = new JsonParser().parse(result)
							.getAsJsonArray();

					// save result in the list
					Intersection[] ints = gson.fromJson(jsonResponse, Intersection[].class);
					intersections = Arrays.asList(ints);
					// conversion back to the right list
					intersections = new ArrayList(intersections);
					
					// display intersection
					// display the location on the map
					for(Intersection int1 : ints) {
						BitmapDescriptor yellowMarker = BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
						mMap.addMarker(new MarkerOptions().position(
							new LatLng(int1.getLat(), int1.getLon()))
							.title("Intersection")
							.snippet("lat: " + int1.getLat() + ", lon: " + int1.getLon())
							.icon(yellowMarker));
					}
				} catch (IllegalStateException e) {
					Log.w("JSON2TEAM",
							"Server response - some error occurred.");
				}
			}
		};

		t.execute(adv);
	}
	
	/**
	 * Sends the measurement information to the web API. Do not call twice, it
	 * should be able to handle errors.
	 */
	private void sendMeasurementInfoToNet(Date time) {
		AsyncTask<Date, Integer, String> t = new AsyncTask<Date, Integer, String>() {
			@Override
			protected String doInBackground(Date... params) {

				Measurement measurement = new Measurement(adv_id, team.getId(),
						params[0], 0, false);

				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
				Gson gson = gsonBuilder.create();
				String jsonToPost = gson.toJson(measurement, Measurement.class);
				PostJson postJson = new PostJson(
						StartScreenActivity.ADVENTURE_URL + "/" + adv_id
								+ "/measurements", jsonToPost);
				postJson.run();
				return postJson.getResponce();
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				/*
				 * if (!result.equals(null)) { continueRunning(null); return; }
				 */

				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					JsonArray jsonResponse = new JsonParser().parse(result)
							.getAsJsonArray();
				} catch (IllegalStateException e) {
					Log.w("JSON2TEAM",
							"Server response: duplicate positi a poslal som on");
				}

				if (handler != null && syncRunnable != null)
					handler.postDelayed(syncRunnable, 5000);
			}
		};

		t.execute(time);
	}

	OnTimeSetListener onTimeSetListener = new OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
			cal.set(Calendar.MINUTE, minute);
			cal.set(Calendar.SECOND, 0);

			// send to web and then start
			sendMeasurementInfoToNet(cal.getTime());
			setMeasurementTime(cal.getTime());
		}
	};

	private void openTimePickerDialog(boolean is24r) {

		Calendar calendar = Calendar.getInstance();
		timePickerDialog = new TimePickerDialog(this, onTimeSetListener,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), is24r);

		timePickerDialog.setTitle("Measurement start at:");
		timePickerDialog.setCancelable(true);
		timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						isTimeSet = false;
						timePickerDialog.cancel();
					}
				});
		timePickerDialog.setMessage("Pick the start time:");
		timePickerDialog.show();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
			@Override
			public boolean onMyLocationButtonClick() {
				mCurrentLocation = mLocationClient.getLastLocation();
				LatLng coordinate = new LatLng(mCurrentLocation.getLatitude(),
						mCurrentLocation.getLongitude());
				CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(
						coordinate, 5);
				mapFragment.getMap().animateCamera(yourLocation);

				CameraPosition cameraPosition = new CameraPosition.Builder()
						.target(coordinate) // Sets the center of the map to
											// Mountain View
						.zoom(16) // Sets the zoom
						.tilt(30) // Sets the tilt of the camera to 30 degrees
						.build(); // Creates a CameraPosition from the builder
				mapFragment.getMap().animateCamera(
						CameraUpdateFactory.newCameraPosition(cameraPosition));
				zoomToMyLocationAtStart = true;
				return false;
			}
		});

		syncRunnable = new Runnable() {

			@Override
			public void run() {

				if (!isNetworkAvailable()) {
					onlineIndicator.setText("DISCONNECTED");
					onlineIndicator.setTextColor(Color.RED);
				} else {
					onlineIndicator.setText("ONLINE");
					onlineIndicator.setTextColor(Color.GREEN);
				}
				if (!postMyCurrentPosition()) {
					return;
				}
				get2TeamCurrentPosition();

				// see you at continueRunning() :)
			}

			private void continueRunning(Position[] pos2Team) {
				getNewMeasurements();
			}

			private void continueRunning2() {
				// repeat the task
				if (handler != null && syncRunnable != null)
					handler.postDelayed(syncRunnable, 5000);
			}

			/**
			 * Gets new measurements from the server.
			 */
			private void getNewMeasurements() {
				AsyncTask<String, Integer, String> at = new AsyncTask<String, Integer, String>() {
					@Override
					protected String doInBackground(String... params) {
						GetJson getJson = new GetJson();
						getJson.setUrl(params[0]);
						getJson.run();
						return getJson.getJson();
					}

					@Override
					protected void onPostExecute(String result) {
						super.onPostExecute(result);

						/*
						 * if (!result.equals(null)) { continueRunning(null);
						 * return; }
						 */

						GsonBuilder gsonBuilder = new GsonBuilder();
						gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
						Gson gson = gsonBuilder.create();

						try {
							JsonArray jsonResponse = new JsonParser().parse(
									result).getAsJsonArray();
							Measurement[] meases = gson.fromJson(jsonResponse,
									Measurement[].class);

							for (Measurement m1 : meases) {
								// if the start time is after the new date
								if (m1.getStart_time().after(new Date())) {
									// run the measurement on this side
									// for all the players
									if (!isTimeSet) {
										Calendar cal = Calendar.getInstance();
										cal.setTime(m1.getStart_time());
										setMeasurementTime(
												cal.get(Calendar.HOUR_OF_DAY),
												cal.get(Calendar.MINUTE));
									}

									// there SHOULD NOT be two measurements for
									// the same thing,
									// so if there is another, well... no. :p
									break;
								}
							}

						} catch (IllegalStateException e) {
							Log.w("JSON2TEAM",
									"Server response: duplicate position");
						} finally {
							continueRunning2();
						}
					}
				};

				at.execute(StartScreenActivity.ADVENTURE_URL + "/"
						+ adv.getId() + "/measurements");

			}

			private void get2TeamCurrentPosition() {
				// TODO: Please rewrite and optimize this ugly code. It was
				// written very straight forward.
				// TODO: ESPECIALLY GETJSON!!!

				AsyncTask<String, Integer, String> at = new AsyncTask<String, Integer, String>() {
					@Override
					protected String doInBackground(String... params) {
						GetJson getJson = new GetJson();
						getJson.setUrl(params[0]);
						getJson.run();
						return getJson.getJson();
					}

					@Override
					protected void onPostExecute(String result) {
						super.onPostExecute(result);

						/*
						 * if (!result.equals(null)) { continueRunning(null);
						 * return; }
						 */

						GsonBuilder gsonBuilder = new GsonBuilder();
						gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
						Gson gson = gsonBuilder.create();

						try {
							JsonArray jsonResponse = new JsonParser().parse(
									result).getAsJsonArray();

							Position[] pos2Team = gson.fromJson(jsonResponse,
									Position[].class);

							for (Position position : pos2Team) {
								teamPositions.put(position.getTeam_id(),
										position);
								Log.d("JSONMY Position of team :0",
										String.valueOf(position.getLat()));
								Log.d("JSONMY Position of team :0",
										String.valueOf(position.getLon()));

								if (team.getId() == position.getTeam_id())
									continue;

								LatLng latLng = new LatLng(position.getLat(),
										position.getLon());

								if (!teamMarkers.containsKey(position
										.getTeam_id())) {
									teamMarkers.put(position.getTeam_id(), mMap
											.addMarker(new MarkerOptions()
													.position(latLng)));
								} else {
									Marker mark = teamMarkers.get(position
											.getTeam_id());
									mark.setPosition(latLng);
								}
							}

							continueRunning(pos2Team);
						} catch (IllegalStateException e) {
							Log.w("JSON2TEAM",
									"Server response: duplicate position");
							continueRunning(null);
						}
					}
				};

				at.execute(StartScreenActivity.ADVENTURE_URL + "/"
						+ adv.getId() + "/positions");

			}

			private boolean postMyCurrentPosition() {
				try {
					mCurrentLocation = mLocationClient.getLastLocation();
				} catch (IllegalStateException e) {
					return false;
				}
				float accuracy = mCurrentLocation.getAccuracy();

				Date timestamp = new Date();
				Log.d("ID POST", String.valueOf(adv_id));
				Position pos = new Position(adv.getId(), team.getId(),
						seq_num++, timestamp, mCurrentLocation, azimuth,
						accuracy, false);

				GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.setDateFormat("yyyy-MM-dd HH:mm:ss");
				Gson gson = gsonBuilder.create();
				String jsonToPost = gson.toJson(pos);
				PostJson postJson = new PostJson(
						StartScreenActivity.ADVENTURE_URL + "/" + adv.getId()
								+ "/positions", jsonToPost);
				Thread thread = new Thread(postJson);
				thread.start();

				Log.d("JSONMYTOPOST", jsonToPost);

				return true;
			}
		};
		// synchronization.run();
		handler.post(syncRunnable);

	}

	@Override
	public void onDisconnected() {
		mLocationClient.disconnect();

		if (syncRunnable != null) {
			syncRunnable = null;
		}
	}

	private void displayToast(String text) {
		Toast.makeText(this, new StringBuilder().append(text),
				Toast.LENGTH_SHORT).show();
	}

	public class MyCountDownTimer extends CountDownTimer {
		public MyCountDownTimer(long startTime, long interval) {
			super(startTime, interval);
		}

		@Override
		public void onFinish() {
			// get line for each team
			// display line for every single citizen

			// BTW, let's try out this new cool "map" type!
			if (teamPolylines.size() > 1) {
				Polyline[] teamLine = teamPolylines.values().toArray(
						new Polyline[teamPolylines.size()]);

				// TODO: just for the first two lines, sorry
				LatLng point1S = teamLine[0].getPoints().get(0);
				LatLng point1E = teamLine[0].getPoints().get(1);
				LatLng point2S = teamLine[1].getPoints().get(0);
				LatLng point2E = teamLine[1].getPoints().get(1);

				LatLng intersection = coordinateOfIntersectionOf2Lines(point1S,
						point1E, point2S, point2E);

				if (intersection != null) {
					// display the location on the map
					BitmapDescriptor yellowMarker = BitmapDescriptorFactory
							.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
					mMap.addMarker(new MarkerOptions().position(intersection)
							.title("Intersection")
							.snippet(intersection.toString())
							.icon(yellowMarker));
					Intersection inters_inst = new Intersection(adv.getId(), new Date(),
							0, intersection);
					intersections.add(inters_inst);
					sendIntersectionInfoToNet(inters_inst);
				}

				for (Polyline line : teamPolylines.values()) {
					line.remove();
				}
			}

			displayTime.setVisibility(View.INVISIBLE);
			countDownTimer = null;
			isTimeSet = false;
		}

		@Override
		public void onTick(long millisUntilFinished) {

			int seconds = (int) (millisUntilFinished / 1000) % 60;
			int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);

			// display line for every single citizen
			for (Map.Entry<Integer, Position> entry : teamPositions.entrySet()) {
				// do a new Polyline
				PolylineOptions lineOpts;
				// if it is you, then use your current position
				if (team.getId() == entry.getKey()) {
					mCurrentLocation = mLocationClient.getLastLocation();
					lineOpts = new PolylineOptions();
					lineOpts.add(
							new LatLng(mCurrentLocation.getLatitude(),
									mCurrentLocation.getLongitude())).add(
							getCoordinatesFromDistanceAndAzimut(
									mCurrentLocation.getLatitude(),
									mCurrentLocation.getLongitude(), 2,
									(float) azimuth));
				} else { // else use whatever you have in the memory
					lineOpts = new PolylineOptions();
					lineOpts.add(
							new LatLng(entry.getValue().getLat(), entry
									.getValue().getLon())).add(
							getCoordinatesFromDistanceAndAzimut(entry
									.getValue().getLat(), entry.getValue()
									.getLon(), 2, (float) entry.getValue()
									.getDirection()));
				}

				// if there exists a Polyline
				if (teamPolylines.containsKey(entry.getKey())) {
					// delete it first
					teamPolylines.get(entry.getKey()).remove();
				}
				// create new polyline
				Polyline line = mMap.addPolyline(lineOpts);
				line.setColor(Color.MAGENTA);
				teamPolylines.put(entry.getKey(), line);
			}

			if (seconds < 10 && minutes < 10)
				displayTime.setText("0" + String.valueOf(minutes) + " : 0"
						+ String.valueOf(seconds));
			else if (minutes < 10)
				displayTime.setText("0" + String.valueOf(minutes) + " : "
						+ String.valueOf(seconds));
			else if (seconds < 10)
				displayTime.setText("" + String.valueOf(minutes) + " : 0"
						+ String.valueOf(seconds));
			else
				displayTime.setText("" + String.valueOf(minutes) + " : "
						+ String.valueOf(seconds));
		}

	}

	private void startCountDownTimer(long milliseconds) {
		if (countDownTimer != null)
			return;
		countDownTimer = new MyCountDownTimer(milliseconds, interval);
		countDownTimer.start();
	}

	private static SensorEventListener mySensorEventListener = new SensorEventListener() {

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			// angle between the magnetic north directio
			// 0=North, 90=East, 180=South, 270=West
			azimuth = event.values[0];

		}
	};

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}