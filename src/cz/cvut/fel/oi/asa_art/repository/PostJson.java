package cz.cvut.fel.oi.asa_art.repository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class PostJson implements Runnable {
	String jSonToPost;
	boolean result;
	String url;
	String responce;

	public String getResponce() {
		return this.responce;
	}

	public void setResponce(String responce) {
		this.responce = responce;
	}

	public PostJson(String url, String jSonToPost) {
		super();
		this.url = url;
		this.jSonToPost = jSonToPost;
	}

	public void setjSonToPost(String jSonToPost) {
		this.jSonToPost = jSonToPost;
	}

	@Override
	public void run() {
		
		int TIMEOUT_MILLISEC = 10000; // = 10 seconds
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(this.url);
		try {
			
			StringEntity strEnt = new StringEntity(this.jSonToPost, "UTF-8");
			strEnt.setContentType("application/json");
			request.setEntity(strEnt);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			HttpResponse response = client.execute(request);
			this.responce = GetJson.convertInputStreamToString(response.getEntity().getContent());
			Log.d("RESPONCE", responce);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}