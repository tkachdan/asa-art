package cz.cvut.fel.oi.asa_art;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cz.cvut.fel.oi.asa_art.adapters.AdventureArrayAdapter;
import cz.cvut.fel.oi.asa_art.fragment.CreateAdventureDialogFragment;
import cz.cvut.fel.oi.asa_art.map.MapScreenActivity;
import cz.cvut.fel.oi.asa_art.model.Adventure;
import cz.cvut.fel.oi.asa_art.model.Team;
import cz.cvut.fel.oi.asa_art.repository.GetJson;
import cz.cvut.fel.oi.asa_art.repository.PostJson;

public class StartScreenActivity extends FragmentActivity
	implements CreateAdventureDialogFragment.CreateAdventureDialogListener {
	
	public static String ADVENTURE_URL = "http://leyfi.felk.cvut.cz/asa-art/adventure";
	ListView listView;
	boolean isConnected = false;
	
	Adventure adv;
	Team team;
	
	int adv_id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start_screen);
		
		getActionBar().setDisplayHomeAsUpEnabled(false);

		team = (Team) getIntent().getSerializableExtra(LoginActivity.EXTRA_TEAM);
		String email = getIntent().getExtras().getString(LoginActivity.EXTRA_EMAIL);
		
		getActionBar().setTitle(email);
		
		listView = (ListView) findViewById(R.id.list);
		GetJson getJS = new GetJson();
		getJS.setUrl("http://leyfi.felk.cvut.cz/asa-art/adventure");
		String json = "null";
		Thread thread = new Thread(getJS);
		thread.start();
		do {
			for (int i = 0; i < 5; i++) {
				try {
					thread.sleep(1000);
					json = getJS.getJson();
					if(!json.equals(null)){
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} while (isConnected);

		String jsonAdventures = json;
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyyy-mm-dd hh:mm:ss");
		Gson gson = gsonBuilder.create();
		Adventure[] advs = gson.fromJson(jsonAdventures, Adventure[].class);

		ArrayAdapter<Adventure> adapter = new AdventureArrayAdapter(this, advs);

		// Assign adapter to ListView
		listView.setAdapter(adapter);

		// ListView Item Click Listener
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item value
				adv = (Adventure) listView
						.getItemAtPosition(position);

				// Show Alert
				Toast.makeText(
						getApplicationContext(),
						"id :" + (position+1) + "  Adventure : "
								+ adv.getTitle(), Toast.LENGTH_LONG)
						.show();
				startMapActivity();

			}

		});

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		findViewById(R.id.start_new_meassurment).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						DialogFragment dialog =
								new CreateAdventureDialogFragment();
						dialog.show(getSupportFragmentManager(), "CreateAdventureDialogFragment");
					}
				});

	}

	public void startMapActivity() {
		Intent intent = new Intent(this, MapScreenActivity.class);
		intent.putExtra("adv", adv);
		intent.putExtra(LoginActivity.EXTRA_TEAM, team);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start_screen, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.log_out) {
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			this.finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

	}


	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		Dialog dialogView = dialog.getDialog();
		EditText title = (EditText) dialogView.findViewById((R.id.editTextStartTitle));
		String titleStr = title.getText().toString();
		EditText batSpecies = (EditText) dialogView.findViewById(R.id.editTextStartBatSpecies);
		String speciesStr = batSpecies.getText().toString();
		EditText frequency = (EditText) dialogView.findViewById(R.id.editTextStartTagFrequency);
		Double frequencyDouble = Double.parseDouble(frequency.getText().toString());
		// TODO: TEST date of creation.

		
		Adventure adv = new Adventure(titleStr, speciesStr, frequencyDouble, 1);
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("yyyy-mm-dd hh:mm:ss");
		Gson gson = gsonBuilder.create();
		String jsonToPost = gson.toJson(adv);
		PostJson postJson = new PostJson(ADVENTURE_URL, jsonToPost);
		Thread thread = new Thread(postJson);
		thread.start();
		
		
		
		startMapActivity();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// DO NOTHING! :)
	}

}