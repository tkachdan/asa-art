-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `asa_adventure`;
CREATE TABLE `asa_adventure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `bat_species` varchar(255) NOT NULL,
  `frequency` double DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ended_on` timestamp NULL DEFAULT NULL,
  `admin_team_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_team_id` (`admin_team_id`),
  CONSTRAINT `asa_adventure_ibfk_1` FOREIGN KEY (`admin_team_id`) REFERENCES `asa_team` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `asa_adventure` (`id`, `title`, `bat_species`, `frequency`, `created_on`, `ended_on`, `admin_team_id`) VALUES
(1,	'Robeblabla',	'Myotis aurascens',	NULL,	'2014-05-11 10:37:25',	NULL,	1),
(2,	'barbar',	'Barbastella barbastellus',	125.75,	'2014-05-11 14:13:51',	NULL,	2),
(3,	'myobec',	'Myotis bechsteini',	150.25,	'2014-05-11 15:04:40',	NULL,	1),
(4,	'eptnil',	'Eptesicus nilssonii',	151.5,	'2014-05-11 15:05:17',	NULL,	1),
(5,	'testTitle',	'bily netopyr',	NULL,	'2014-05-11 15:13:46',	NULL,	2),
(7,	'Test',	'Test species , hura , bitch',	12,	'2014-05-14 14:49:59',	NULL,	NULL),
(10,	'hdhdhd',	'jfhfhfh',	737363,	'0000-00-00 00:00:00',	NULL,	1),
(12,	'test',	'reset vale yes ',	5342.6,	'2014-09-16 00:09:03',	NULL,	1),
(13,	'eeee',	'eeeee',	3333,	'2014-09-16 00:09:47',	NULL,	1),
(14,	'teste',	'teatetest',	555252,	'0000-00-00 00:00:00',	NULL,	1);

DROP TABLE IF EXISTS `asa_adventure_team`;
CREATE TABLE `asa_adventure_team` (
  `adventure_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `adventure_id` (`adventure_id`),
  KEY `team_id` (`team_id`),
  CONSTRAINT `asa_adventure_team_ibfk_1` FOREIGN KEY (`adventure_id`) REFERENCES `asa_adventure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `asa_adventure_team_ibfk_2` FOREIGN KEY (`team_id`) REFERENCES `asa_team` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `asa_adventure_team` (`adventure_id`, `team_id`, `id`) VALUES
(1,	1,	1),
(1,	2,	2),
(3,	2,	3),
(2,	1,	4),
(4,	1,	5);

DROP TABLE IF EXISTS `asa_intersection`;
CREATE TABLE `asa_intersection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adventure_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seq_num` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `accuracy_hor` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `adventure_id` (`adventure_id`),
  CONSTRAINT `asa_intersection_ibfk_2` FOREIGN KEY (`adventure_id`) REFERENCES `asa_adventure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `asa_intersection_position`;
CREATE TABLE `asa_intersection_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intersection_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `intersection_id` (`intersection_id`),
  KEY `position_id` (`position_id`),
  CONSTRAINT `asa_intersection_position_ibfk_1` FOREIGN KEY (`intersection_id`) REFERENCES `asa_intersection` (`id`) ON DELETE CASCADE,
  CONSTRAINT `asa_intersection_position_ibfk_2` FOREIGN KEY (`position_id`) REFERENCES `asa_position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `asa_measurement`;
CREATE TABLE `asa_measurement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adventure_id` int(11) NOT NULL,
  `initiating_team_id` int(11) DEFAULT NULL,
  `start_time` datetime NOT NULL,
  `duration` int(11) NOT NULL,
  `ended` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `adventure_id` (`adventure_id`),
  KEY `initiating_team_id` (`initiating_team_id`),
  CONSTRAINT `asa_measurement_ibfk_1` FOREIGN KEY (`adventure_id`) REFERENCES `asa_adventure` (`id`) ON DELETE CASCADE,
  CONSTRAINT `asa_measurement_ibfk_2` FOREIGN KEY (`initiating_team_id`) REFERENCES `asa_team` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `asa_position`;
CREATE TABLE `asa_position` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `adventure_id` int(11) DEFAULT NULL,
  `team_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `seq_num` int(11) NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `alt` float NOT NULL,
  `accuracy_hor` float NOT NULL,
  `accuracy_ver` float NOT NULL,
  `direction` float NOT NULL,
  `accuracy_dir` float NOT NULL,
  `was_measured` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `team_id_adventure_id_seq_num` (`team_id`,`adventure_id`,`seq_num`),
  KEY `adventure_id` (`adventure_id`),
  CONSTRAINT `asa_position_ibfk_3` FOREIGN KEY (`adventure_id`) REFERENCES `asa_adventure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `asa_position_ibfk_5` FOREIGN KEY (`team_id`) REFERENCES `asa_team` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `asa_team`;
CREATE TABLE `asa_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(64) NOT NULL,
  `role` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `asa_team` (`id`, `full_name`, `email`, `password`, `role`) VALUES
(1,	'Pavel Valach',	'valacpav@fel.cvut.cz',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	'admin'),
(2,	'Daniel Tkachenko',	'tkachdan@fel.cvut.cz',	'7c4a8d09ca3762af61e59520943dc26494f8941b',	'admin');

-- 2014-05-16 13:30:24
